<?php
 /*
  Copyright 2014 Douglas Mason

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
session_start();
$dbhost = 'insert host name here';
$dbname = 'insert database instance here';
$dbuser = 'insert username here';
$dbpass = 'insert password here';
$mysqli = new mysqli($dbhost,$dbuser,$dbpass,$dbname) or die("Error connecting the database");
  $user = $_SESSION['username'];
  $results = $mysqli->query("select * from `preferences` where `username`=\"$user\"");
  if($results->num_rows==1){
    $obj = $results->fetch_object();
    $id = $obj->id;
    var_dump($obj);
    echo "Inserting data<br>";
    if(isset($_POST['search'])&&$_POST['search']!=''){
      echo $_POST['search']."<br>";
      $searchprepare = $mysqli->prepare("update `preferences` SET `search`= ? where `id`=$id");
      $searchprepare->bind_param("s",htmlspecialchars($_POST['search']));
      $result=$searchprepare->execute();
      if($result=FALSE){
        echo "failed to update info.";
      }
      else{
        echo "Successfully updated search query to ".$_POST['search']."<br>";
      }
    }
    if(isset($_POST['numvids'])&&$_POST['numvids']!=0){
      echo "updating video number";
      $searchprepare = $mysqli->prepare("update `preferences` SET `numvids`= ? where `id`=$id");
      $searchprepare->bind_param("i",$_POST['numvids']);
      $result = $searchprepare->execute();
      if($result=FALSE){
        echo "failed to update info.";
      }
      else{
        echo "Successfully updated number of videos to ".$_POST['numvids']."<br>";
      }
    }
    if(isset($_POST['numpics']) && $_POST['numpics']!=0){
      $searchprepare = $mysqli->prepare("update `preferences` SET `numpics`= ? where `id`=$id");
      $searchprepare->bind_param("i",$_POST['numpics']);
      $result = $searchprepare->execute();
      if($result=FALSE){
        echo "failed to update info.";
      }
      else{
        echo "Successfully updated number of pictures to ".$_POST['numpics']."<br>";
      }
    }
  }
?>
