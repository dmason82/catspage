<?php
 /*
 new_user.php - Creates new users for the cat experience page.
   Copyright 2014 Douglas Mason
 Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
  if (!isset($_SERVER["HTTP_HOST"])) {
  parse_str($argv[1], $_GET);
  parse_str($argv[1], $_POST);
}
$dbhost = 'insert host name here';
$dbname = 'insert database instance here';
$dbuser = 'insert username here';
$dbpass = 'insert password here';
$mysqli = new mysqli($dbhost,$dbuser,$dbpass,$dbname) or die("Error connecting the database");
session_start();
if($_.POST){
  //do post like operations
  if(validate_password()){
    $userid = $mysqli->real_escape_string(stripslashes($_POST['username']));
    $userpass = $_POST['password'];
    if(ctype_alnum($userid)){
      $result = $mysqli->query("select * from logon where username='$userid'");
      if($result->num_rows==0){
        //YES! let's insert this bad boy into the database.
        $prepared = $mysqli->prepare("insert into `logon` (username,password) values(?,?)");
        if($prepared==TRUE){
          $prepared->bind_param("ss",$userid,sha1($userpass));
          $prepared->execute();
          $result_row = $mysqli->insert_id;
          if($result_row!=0){
            $prefsprepare = $mysqli->prepare("insert into `preferences`(username) values(?)");
            $prefsprepare->bind_param("s",$userid);
            $prefsprepare->execute();
            echo "You have successfully registered.<br> Perhaps, you would like to <a href='login.php'>Login</a>";
          }
        }
        else{
          echo "Could not prepare SQL statement, check for validity";
        }
      }
      else{
        //non-unique username
        echo "Sorry, but that username is already taken, please choose another.!";
      }
    }
    else{
      //Fail! non alpha-numeric.
      echo "Please create a username with alphanumeric characters only!";
    }
  }
  else{
    //This is where we do some fancy AJAX thing... eventually
    echo "Please verify that your password is at least 8 characters, has 1 uppercase, 1 lowercase, and 1 number.";

  }
}

function validate_password(){
  $passcandidate = $_POST['password'];
  $uppers = "/[A-Z]/";
  $lowers = "/[a-z]/";
  $numbers = "/[0-9]/";
  $result = TRUE;
  if(strlen($passcandidate)<8){
    //WTF not long enough!
    $result=FALSE;
  }
  if(preg_match_all($uppers,$passcandidate,$out)<1){
    //no uppers
    $result=FALSE;
  }
  if(preg_match_all($lowers,$passcandidate,$out)<1){
    $result=FALSE;
  }
  if(preg_match_all($numbers,$passcandidate,$out)<1){
    //no numbers!
    $result=FALSE;
  }
  return $result;
}
?>
