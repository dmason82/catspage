<?php
 /*
 Settings.php -Frontend for the settings for the cat experience page. 
 Copyright 2014 Douglas Mason

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
session_start();
if(!session_is_registered(username)){
  header("location:login.php");
}
?>
<!DOCTYPE HTML>
<html>
<head>
  <meta charset="utf-8">
  <link type="text/css" rel="stylesheet" href="settings.css">
  <script src="jquery-1.11.0.js"></script>
  <title>Settings for the Cat Page</title>
</head>
<body>
<div id="error">
</div>
  <h2>Settings for <?php echo $_SESSION['username'];?> </h2>
  <form action="savesettings.php" method="post" id="settings">
    <label for="srch" >Search query for YouTube Player</label>
    <input type="text" name="search" placeholder="Cats" id="srch"><br>
    <label for="nv">Number of videos for YouTube Player</label>
    <input type="number" name="numvids" id="nv"><br>
    <label for="np">Number of cat pictures to display</label>
    <input type="number" name="numpics" id="np"><br>
    <input type="submit" value="Update preferences">
  </form>
  <br>
  <a href="loggedin.php">Back to main site</a>
  <br>
  <a href="logout.php">Logout</a>
  <script>
  $("#settings").submit(function() {
    var url = "savesettings.php"; // the script where you handle the form input.
    $.ajax({
           type: "POST",
           url: url,
           data: $("#settings").serialize(),
           success: function(data)
           {
             $("#error").html(data);
           }
         }).fail(function(data){alert("Failed!");});

    return false; // avoid to execute the actual submit of the form.
  });
  </script>
</body>
</html>
