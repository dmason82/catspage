CatsPage
========

A PHP page that is a mashup of various cat-related APIs and a youtube player once authenticated with MySQL to persist search settings and an override for the YouTube player

Includes the following libraries:
jQuery 1.11.0
jQuery UI 
Google API for PHP
jQuery TubePlayer - v1.1.7

Also note, that you will need a Google API key for YouTube access, and to create a MySQL database instance that allows for database.sql to be executed.
