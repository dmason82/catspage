<?php
ini_set("allow_url_fopen", true);
session_start();
$dbhost = 'dbhost';
$dbname = 'databasename';
$dbuser = 'dbuser';
$dbpass = 'dbpassword';
?>
<!doctype html>
<!--login.php - Source file for input files for post values
The MIT License (MIT)
Copyright (c) 2014 Doug Mason

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

-->

<html>
<head>
<meta charset='utf-8'>
<title>Login:The Cat Experience</title>
<script src="jquery-1.11.0.js"></script>
<script src="jquery-ui/ui/jquery-ui.js"> </script>
<link type="text/css" rel="stylesheet" href="input.css">
<link type="text/css" rel="stylesheet" href="jquery-ui/themes/base/jquery.ui.tooltip.css">
<script>
 $(function() {
    $( document ).tooltip();
  });
</script>
</head>
<body>
  <div id="error">
  </div>
<H3>Please login using a username and password.</H3>
<form action = "login.php" method="post" id="login">
  <label for="user">Username</label>
<input type="text" name="username" id="user" placeholder="username">
<label for="pass">Password</label>
<input type="password" id="pass" name="password">
<input type="submit" name="login" value="Login">
</form>
<div id="register">
<a href="register.php">Sign up!</a>
</div>
<br>
<script>
$("#login").submit(function() {
  var url = "session.php"; // the script where you handle the form input.
  var data=$('#login').serialize();
  $.ajax({
         type: "POST",
         url: url,
         data: $("#login").serialize(),
         success: function(data)
         {
           if(data == "success!!!"){
             window.location.href = "loggedin.php";
           }
         }
       }).fail(function(data){alert("Failed!");});

  return false; // avoid to execute the actual submit of the form.
});
</script>
<img src="http://placekitten.com/350/200" alt="A kitten, staring at you" id="kitten" title="Meow!">
<div id="catfact">
  <h5>And for some random cat facts...</h5>
<?php
$catjsontext = file_get_contents('http://catfacts-api.appspot.com/api/facts?number=3');
$catjsonobject = json_decode($catjsontext);
$catarray = $catjsonobject->{"facts"};
 foreach($catarray as $key=>$value){
   echo($catarray[$key]."<br>");
 }

?>
</div>
</html>
