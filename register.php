<?php
 /*
 Register.php  - Registration page for the cat experience
  Copyright 2014 Douglas Mason

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
session_start();

?>
<!DOCTYPE HTML>
<html>
<head>
<!--
-->
<meta charset='utf-8'>
<title>Catty registration</title>
<script src="jquery-1.11.0.js"></script>
<script src="jquery-ui/ui/jquery-ui.js"> </script>
<link type="text/css" rel="stylesheet" href="register.css">
</head>
<body>
  <div id="error">
  </div>
  <div id="requirements">
    Please create a username with at least 4 alphanumeric characters<br>
    As well as a password with at least 8 characters<br>
    Your password must also contain 1 upper-case character, 1 lower-case
    character and 1 number. <br>
  </div>
  <a href="login.php">Back to login page</a>
  <div id="registration_header">
    <h3>Registration form</h3>
  </div>
<form action = "new_user.php" method="post" id="register">
    <label for="user">Username</label>
  <input type="text" name="username" placeholder="username" id="user">
  <label for="pass">Password</label>
  <input type="password" name="password" id="pass">
  <input type="submit" name="login" value="Register">
  </form>
  <script>
  $("#register").submit(function() {
    var url = "new_user.php"; // the script where you handle the form input.
    var data=$('#register').serialize();
    $.ajax({
           type: "POST",
           url: url,
           data: $("#register").serialize(),
           success: function(data)
           {
             $("#error").html(data);
           }
         }).fail(function(data){alert("Failed!");});

    return false; // avoid to execute the actual submit of the form.
});
</script>
  <br>
  <br>
  <img src="http://placekitten.com/400/300" alt="A kitten, staring at you">
</body>
</html>
