<?php
 /*
 loggedin.php - authenticated experience for Cats page
  Copyright 2014 Douglas Mason
 Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
 session_start();
 ini_set("allow_url_fopen", true);
// Call set_include_path() as needed to point to your client library.
require_once 'Google/Client.php';
require_once 'Google/Service/YouTube.php';
if(!session_is_registered(username)){
  header("location:login.php");
}

//if we have a session, we will also check for preferences, otherwise set defaults.
$dbhost = 'insert host name here';
$dbname = 'insert database instance here';
$dbuser = 'insert username here';
$dbpass = 'insert password here';
$mysqli = new mysqli($dbhost,$dbuser,$dbpass,$dbname) or die("Error connecting the database");
$user = $_SESSION['username'];
$results = $mysqli->query("select * from `preferences` where `username`=\"$user\"");
if($results->num_rows==1){
  $obj = $results->fetch_object();
  if(isset($obj->search) && $obj->search!='' ){
    $ytsearch = $obj->search;
  }
  if(isset($obj->numvids) && $obj->numvids!=0){
    $numvids=$obj->numvids;
  }
  if(isset($obj->numpics) && $obj->numpics!=0){
    $numpics=$obj->numpics;
  }
}
else{
  $ytsearch="kittens";
  $numvids = 10;
  $numpics = 4;
}

?>

<!DOCTYPE HTML>
<html>
<head>
  <meta charset="utf-8">
  <script src="jquery-1.11.0.js"></script>
  <script src="jQuery.tubeplayer.js"></script>
  <link type="text/css" rel="stylesheet" href="loggedin.css">
  <title>Cats, the logged in experience</title>
</head>
<body>
<script>
var updateVid=function(video){
  jQuery("#player").tubeplayer("play",video);
}
</script>
<?php
$htmlbody = "";
$videos = '';
$players = array();
$api_key = "GOOGLE API Key here";
$client = new Google_Client();
$client->setDeveloperKey($api_key);
$youtube = new Google_Service_YouTube($client);
try{
    $searchResponse = $youtube->search->listSearch('id,snippet', array(
      'q' => $ytsearch,
      'maxResults' => $numvids
    ));
    foreach ($searchResponse['items'] as $searchResult) {
      if($searchResult['id']['kind'] == 'youtube#video') {
           $videos .= sprintf('<tr><td>%s</td><td><input type=button value="load" onclick="updateVid(\'%s\');"></td></tr>',
               $searchResult['snippet']['title'], $searchResult['id']['videoId']);
               array_push($players, $searchResult['id']['videoId']);
      }

    }

}
catch (Google_ServiceException $e) {
    $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
      htmlspecialchars($e->getMessage()));
  } catch (Google_Exception $e) {
    $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
      htmlspecialchars($e->getMessage()));
  }

  echo $htmlBody;
?>

<?php
echo "<h1> Welcome, $user!</h1>\n";
echo "Search criteria: $ytsearch <br>";
echo "Maximum video results: $numvids<br>";
echo "Number of pictures: $numpics<br>";
echo "<br><a href=\"settings.php\">Settings?</a><br>";
echo "<br><a href=\"logout.php\">Logout?</a><br>";
echo "<div id='player'></div>";
$vid = $players[0];
echo "<script>
jQuery(\"#player\").tubeplayer({
  width: 600, // the width of the player
  height: 450, // the height of the player
  allowFullScreen: \"true\", // true by default, allow user to go full screen
  initialVideo: \"$vid\", // the video that is loaded into the player
  preferredQuality: \"default\",
  onPlay: function(id){},
  onPause: function(){},
  onStop: function(){},
  onSeek: function(time){}, // after the video has been seeked to a defined point
  onMute: function(){}, // after the player is muted
  onUnMute: function(){} // after the player is unmuted
});
</script>";
echo "<table> $videos</table>";
$catsxml = simplexml_load_file("http://thecatapi.com/api/images/get?format=xml&results_per_page=$numpics");
$array = $catsxml->data->images->image;
$url_arr=array();
echo "<table id='catpictable'>\n";
foreach($array as $item){
  array_push($url_arr,$item->url);
  echo "<tr><td><img src='$item->url' alt='$item->source_url' title='Mew'><br>Obtained from: $item->source_url<br></td></tr>\n";
}
echo "</table>";
?>

</html>
