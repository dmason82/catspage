<?php
/*session.php - Controls flow of whether to go to input or domath depending on session and
post values.
 Copyright 2014 Douglas Mason

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
session_start();
$dbhost = 'insert host name here';
$dbname = 'insert database instance here';
$dbuser = 'insert username here';
$dbpass = 'insert password here';
$mysqli = new mysqli($dbhost,$dbuser,$dbpass,$dbname) or die("Error connecting the database");
	if(!empty($_POST['username']) && !empty($_POST['password'])){
		if(strlen($_POST['username'])<4){
			echo "Usernames are more than 4 characters.";
		}
		else if(strlen($_POST['password'])<8){
			echo "Invalid password, they are at least 7 characters long.";
		}
		else{

			$userid = $mysqli->real_escape_string(stripslashes($_POST['username']));
			/*Yes, this isn't really that secure...
			//http://www.php.net/manual/en/faq.passwords.php#faq.passwords.fasthash
			however, the PHP version on ONID is not supported by https://github.com/ircmaxell/password_compat
			Assuming that of course, the 'squeeze' in php -v refers to the Debian release.
			*/
			$userpass = sha1($_POST['password']);
			$result = $mysqli->query("select * from logon where username='$userid' AND password='$userpass';");
			if($result->num_rows==1){
				//SUCCESS
				$_SESSION['username']=$userid;
				echo("success!!!");
				//header("location:loggedin.php");
			}
			else{
				//Authentication fail!
				echo "Invalid username and/or password.. Or have you not <a href='register.php'>registered</a> yet?";

			}
		}
	}
else{
	echo "Please enter a username and password to login.";
}
?>
